from selenium import webdriver
from selenium.webdriver.support.ui import Select
driver = webdriver.Chrome(executable_path=r'C:\TestFiles\chromedriver.exe')
driver.get('https://www.seleniumeasy.com/test/input-form-demo.html')

driver.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[1]/div/div/input').send_keys('Konrad')
driver.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[2]/div/div/input').send_keys('Cisek')
driver.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[3]/div/div/input').send_keys('konrad_cisek@o2.pl')
driver.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[4]/div/div/input').send_keys('555 222 444')
driver.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[5]/div/div/input').send_keys('Lęborska 24')
driver.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[6]/div/div/input').send_keys('Gdańsk')
select = Select(driver.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[7]/div/div/select'))
select.select_by_visible_text('Colorado')
driver.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[8]/div/div/input').send_keys('21-388')
driver.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[9]/div/div/input').send_keys('test_selenium.pl')
driver.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[10]/div/div[2]/label/input').click()
driver.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[11]/div/div/textarea').send_keys('Gotowe Yupp')
driver.find_element_by_xpath('//*[@id="contact_form"]/fieldset/div[13]/div/button').click()
driver.quit()