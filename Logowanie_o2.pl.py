from selenium import webdriver
import time

driver = webdriver.Chrome(executable_path=r'C:\TestFiles\chromedriver.exe')
driver.implicitly_wait(10)

driver.get('https://poczta.o2.pl/zaloguj')
driver.maximize_window()
driver.find_element_by_id('login').send_keys('konrad_cisek@o2.pl')
driver.find_element_by_id('password').send_keys('lordvankonred7')
driver.find_element_by_css_selector('button#login-button.btn.btn--1').click()

time.sleep(5)

driver.find_element_by_css_selector('#highlight_notification_anchor.topuser__wrap').click()

time.sleep(5)

driver.find_element_by_id("Logout-Button").click()
driver.quit()