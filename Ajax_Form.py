from selenium import webdriver
from selenium.webdriver.support.ui import Select
driver = webdriver.Chrome(executable_path=r'C:\TestFiles\chromedriver.exe')
driver.get('https://www.seleniumeasy.com/test/ajax-form-submit-demo.html')

driver.find_element_by_xpath('//*[@id="title"]').send_keys('Konrad Cisek')
driver.find_element_by_xpath('//*[@id="description"]').send_keys('formularz został stworzony na potrzeby przeprowadzenia próbnego testu')
driver.find_element_by_xpath('//*[@id="btn-submit"]').click()