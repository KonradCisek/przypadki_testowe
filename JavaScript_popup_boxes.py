from selenium import webdriver
driver = webdriver.Chrome(executable_path=r'C:\TestFiles\chromedriver.exe')
driver.get('https://www.seleniumeasy.com/test/javascript-alert-box-demo.html')
import time

driver.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[3]/div[2]/button').click() #3 wariant
#driver.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[2]/div[2]/button').click() #2 wariant
#driver.find_element_by_xpath('//*[@id="easycont"]/div/div[2]/div[1]/div[2]/button').click()

alert = driver.switch_to.alert
alert.send_keys("Konrad") #3
time.sleep(2)

alert.accept() #2
#alert_obj.dismiss() #2

#alert_obj.accept() – used to accept the Alert
#alert_obj.dismiss() – used to cancel the Alert
#alert.send_keys() – used to enter a value in the Alert text box.
#alert.text() – used to retrieve the message included in the Alert pop-up