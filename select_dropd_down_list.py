from selenium import webdriver
from selenium.webdriver.support.ui import Select
driver = webdriver.Chrome(executable_path=r'C:\TestFiles\chromedriver.exe')
driver.get('https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html')

select = Select(driver.find_element_by_id('select-demo'))
select.select_by_visible_text('Tuesday')
select = Select(driver.find_element_by_id('multi-select'))
select.select_by_visible_text('Texas')
driver.find_element_by_id('printMe').click()
driver.quit()