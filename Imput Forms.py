#imput forms
from selenium import webdriver

driver = webdriver.Chrome(executable_path=r'C:\TestFiles\chromedriver.exe')
driver.implicitly_wait(5)

driver.get('https://www.seleniumeasy.com/test/basic-first-form-demo.html')
driver.maximize_window()

driver.find_element_by_id("user-message").send_keys('zadanie 1')
driver.find_element_by_css_selector("button.btn.btn-default").click()


driver.find_element_by_id("sum1").send_keys("4")
driver.find_element_by_id("sum2").send_keys("6")
driver.find_element_by_css_selector("button.btn.btn-default").click()
driver.quit()